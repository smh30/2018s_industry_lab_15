package ictgradschool.industry.designpatterns.ex02;

import ictgradschool.industry.designpatterns.ex01.NestingShape;
import ictgradschool.industry.designpatterns.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.Iterator;

public class ShapeTreeAdapter implements TreeModel {

    private NestingShape adaptee;

    public ShapeTreeAdapter(NestingShape root) {
        adaptee = root;
    }

    @Override
    public Object getRoot() {
        return adaptee;
    }

    @Override
    public int getChildCount(Object parent) {
        int result = 0;
        Shape shape = (Shape) parent;

        if (shape instanceof NestingShape) {
            NestingShape nestingShape = (NestingShape) shape;
            result = nestingShape.shapeCount();
        }

        return result;
    }

    @Override
    public Object getChild(Object parent, int index) {
        Object result = null;

        if (parent instanceof NestingShape) {
            NestingShape p = (NestingShape) parent;
            result = p.shapeAt(index);
        }
        return result;
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int indexOfChild = -1;

        if (child instanceof Shape && parent instanceof NestingShape) {
            NestingShape p = (NestingShape) parent;
            Shape c = (Shape) child;
            indexOfChild = p.indexOf(c);
        }

        return indexOfChild;
    }


    @Override
    public boolean isLeaf(Object node) {
        if (node instanceof NestingShape){
            return false;
        }
        return true;
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }


    @Override
    public void addTreeModelListener(TreeModelListener l) {

    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {

    }
}
