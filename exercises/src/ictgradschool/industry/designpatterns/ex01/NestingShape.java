package ictgradschool.industry.designpatterns.ex01;

import java.util.ArrayList;
import java.util.List;

public class NestingShape extends Shape {

    private List<Shape> children;

    public NestingShape() {
//        Creates a NestingShape object with default values for state.
        super();
        children = new ArrayList<>();
    }

    public NestingShape(int x, int y) {
//        Creates a NestingShape object
        super(x, y);
        children = new ArrayList<>();
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
//        Creates a NestingShape with specified values for location, velocity and direction.
//        Non-specified state items take on default values.
        super(x, y, deltaX, deltaY);
        children = new ArrayList<>();
    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
//        Creates a NestingShape with specified values for location, velocity, direction, width
//        and height.
        super(x, y, deltaX, deltaY, width, height);
        children = new ArrayList<>();
    }

    /**
     * Moves a NestingShape object (including its children) with the bounds specified by
     * arguments width and height. Remember that the NestingShape ’s children only want
     * to move within the bounds of the NestingShape itself, rather than the whole screen.
     * @param width width of two-dimensional world.
     * @param height height of two-dimensional world.
     */
    public void move(int width, int height) {
        // first move the nestingshape itself...
        super.move(width, height);

        for (Shape child : children) {
            // move the children, passing in the width and height of the nestingshape
            //rather than the usual width and height of the jpanel
            child.move(this.getWidth(), this.getHeight());
        }
    }

    /**
     *
     * @param painter the ictgradschool.industry.designpatterns.ex01.Painter object used for drawing.
     */
    @Override
    public void paint(Painter painter) {
//        Paints a NestingShape object by drawing a rectangle around the edge of its
//        bounding box. The NestingShape object's children are then painted.

        // first draw the NestingShape, which is basically a rectangle...
        painter.drawRect(fX, fY, fWidth, fHeight);


        //and then paint each of the children
        //translate the origin so that it is the origin of the nestingshape rather than 0,0
        painter.translate(this.fX, this.fY);
        for (Shape child : children) {
            // will this work? will it just figure out what type of shape it is and call the correct painter???
            child.paint(painter);
        }
        //set the origin back to normal
        painter.translate(0 - this.fX, 0 - this.fY);
    }

    public void add(Shape child) throws IllegalArgumentException {
        /*Attempts to add a Shape to a NestingShape object. If successful, a two-way link is
        established between the NestingShape and the newly added Shape. This method
        throws an IllegalArgumentException if an attempt is made to add a Shape to a
        NestingShape instance where the Shape argument is already a child within a
        NestingShape instance. An IllegalArgumentException is also thrown when an
        attempt is made to add a Shape that will not fit within the bounds of the proposed
        NestingShape object.*/
        if (child.parent() != null){
            throw new IllegalArgumentException("this shape is not up for adoption");
        } else if (child.getX() + child.getWidth() > this.getWidth() || child.getY() + child.getHeight() > this.getHeight()) {
            throw new IllegalArgumentException("doesn't fit :( :( :(");
        }

        children.add(child);
        child.setParent(this);



    }

    public void remove(Shape child){
        /*Removes a particular Shape from a NestingShape instance. Once removed, the
        two-way link between the NestingShape and its former child is destroyed. This
        method has no effect if the Shape specified to remove is not a child of the
        NestingShape .*/

        if (children.contains(child)) {
            children.remove(child);
            child.setParent(null);
        }
    }

    public Shape shapeAt(int index) throws IndexOutOfBoundsException{
        /*Returns the Shape at a specified position within a NestingShape . If the position
        specified is less than zero or greater than the number of children stored in the
        NestingShape less one this method throws an IndexOutOfBoundsException .*/

        return children.get(index);
    }

    public int shapeCount(){
       /* Returns the number of children contained within a NestingShape object. Note this
        method is not recursive - it simply returns the number of children at the top level
        within the callee NestingShape object.*/

       return children.size();
    }

    public int indexOf(Shape child){
        /*Returns the index of a specified child within a NestingShape object. If the Shape
        specified is not actually a child of the NestingShape this method returns -1;
        otherwise the value returned is in the range 0 .. shapeCount() - 1.*/

        return children.indexOf(child);
    }

    public boolean contains (Shape child){
//        Returns true if the Shape argument is a child of the NestingShape object on which
//        this method is called, false otherwise.

        return children.contains(child);
    }
}
